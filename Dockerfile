FROM golang:1.8

ADD ./src /go/src/app
WORKDIR /go/src/app
ENV PORT=8100
CMD ["go", "run", "main.go"]
